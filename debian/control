Source: streamlink
Maintainer: Alexis Murzeau <amubtdx@gmail.com>
Section: python
Priority: optional
Standards-Version: 4.6.1
Build-Depends: debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 python3,
 pybuild-plugin-pyproject,
# Streamlink dependencies
 python3-isodate,
 python3-pycountry (>= 16.10.23),
 python3-pycryptodome,
 python3-requests,
 python3-setuptools,
 python3-wheel,
 python3-socks,
 python3-websocket,
 python3-lxml,
# Documentation dependencies
 python3-docutils,
 python3-myst-parser,
 python3-sphinx,
 python3-sphinx-rtd-theme,
# Tests dependencies
 python3-freezegun,
 python3-pytest,
 python3-requests-mock
Homepage: https://streamlink.github.io/
Vcs-Git: https://salsa.debian.org/amurzeau/streamlink.git
Vcs-Browser: https://salsa.debian.org/amurzeau/streamlink
Rules-Requires-Root: no

Package: python3-streamlink
Architecture: all
Depends: ${misc:Depends},
 ${python3:Depends}
Recommends: rtmpdump,
  ffmpeg
Suggests: streamlink, python3-streamlink-doc
Description: Python module for extracting video streams from various websites
 Streamlink is a CLI utility which pipes video streams from various services
 into a video player, such as VLC.
 The main purpose of streamlink is to avoid resource-heavy and unoptimized
 websites, while still allowing the user to enjoy various streamed content.
 .
 Streamlink is a fork of the Livestreamer project.
 .
 Please consider donating or paying for subscription services when they are
 available for the content you consume and enjoy.
 .
 This package makes Streamlink APIs accessible in Python 3.

Package: python3-streamlink-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${sphinxdoc:Depends},
  ${misc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Suggests: python3-streamlink
Description: CLI for extracting video streams from various websites (documentation)
 Streamlink is a CLI utility which pipes video streams from various services
 into a video player, such as VLC.
 The main purpose of streamlink is to avoid resource-heavy and unoptimized
 websites, while still allowing the user to enjoy various streamed content.
 .
 Streamlink is a fork of the Livestreamer project.
 .
 Please consider donating or paying for subscription services when they are
 available for the content you consume and enjoy.
 .
 This package contains the Streamlink usage, API and plugins
 documentation in HTML format.

Package: streamlink
Section: video
Architecture: all
Depends: ${misc:Depends},
  ${python3:Depends},
  python3-streamlink (= ${source:Version})
Recommends: vlc | mpv | mplayer | omxplayer
Description: CLI for extracting video streams from various websites to a video player
 Streamlink is a CLI utility which pipes video streams from various services
 into a video player, such as VLC.
 The main purpose of streamlink is to avoid resource-heavy and unoptimized
 websites, while still allowing the user to enjoy various streamed content.
 .
 Streamlink is a fork of the Livestreamer project.
 .
 Please consider donating or paying for subscription services when they are
 available for the content you consume and enjoy.
